/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import org.w3c.dom.Document;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Aleksi
 */
public class Theaters {
    private String content = "";
    private Document doc;
    private ArrayList<Theater> alist = new ArrayList<Theater>();

    
    public Theaters(){
        try {
            load();
        } catch (IOException ex) {
            Logger.getLogger(Theaters.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("Teatterilistan lataamisessa on tapahtunut virhe.");
        }
    }
    
    // Loads all needed content from URL
    public void load() throws IOException{
        try{
        
            URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
        
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
            String line;
        
            while((line = br.readLine()) != null){
                content += line + "\n";
            }
            buildDoc(); // Builds document
            parseCurrentData(); // Parses data in global variable
        }
        catch(MalformedURLException ex){
            System.err.println("Huono urli");
        }
    }
    
    // Builds document
    private void buildDoc(){
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(Theaters.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    // Parses data in global variable
    private void parseCurrentData(){
        String name;
        String id;
        
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        
        for(int i = 0; i < nodes.getLength();i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
                      
            name = getValue("Name", e);
            id = getValue("ID", e);
            
            alist.add(new Theater(name, id));
                        
        }
    }
    
    private String getValue(String tag, Element e){
        
        //NOTE: Gets text content, not value
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();

    }
    
    public ArrayList<Theater> getAlist() {
        return alist;
    }
    
}

class Theater{
    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
    
    public Theater(String n,String i){
        name = n;
        id = i;
    }
    
    @Override
    public String toString(){
        return name;
    }
}
