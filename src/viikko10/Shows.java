/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Aleksi
 */

public class Shows {
    
    private String content = "";
    
    private Document doc;
    private ArrayList<Show> slist = new ArrayList<Show>();
    
    public Shows(){
        
    }

    public void load(String x, String date, ArrayList<Theater> a) throws MalformedURLException, IOException{
        
        String id = x;
        content = "";
        slist.clear();
        
        //Function needs id, so if empty fill in the default
        if("".equals(id)){
            id = "1029";
        }
        
        //Ready made search terms in URL
        URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + date);

        
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
        String line;
        
        while((line = br.readLine()) != null){
            content += line + "\n";
        }
        
        buildDoc(); // Builds document
        parseCurrentData(); // Parses data in global variable
        
    }
    
    // Builds document
    private void buildDoc(){
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(Theaters.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    // Parses data in global variable
    private void parseCurrentData(){
        String title;
        String start;
        String end;
        String theater;

        NodeList nodes = doc.getElementsByTagName("Show");
                
        for(int i = 0; i < nodes.getLength();i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            
                   
            title = getValue("Title", e);
            start = getValue("dttmShowStart", e);
            end = getValue("dttmShowEnd", e);
            theater = getValue("Theatre", e) + ": \n";
            
            start = formatDate(start);
            end = formatDate(end);
            
            slist.add(new Show(title, start, end, theater));
        }
    }
    
    private String getValue(String tag, Element e){
          
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();

    }
    
    private String formatDate(String s){
        String date;
        
        String d = s.substring(8,10);
        String m = s.substring(5,7);
        String y = s.substring(0,4);
        String time = s.substring(11,16);
                       
        date = d + "." + m + "." + y + " " + time;

        return date;
    }
    
    
    public ArrayList<Show> getSlist() {
        return slist;
    }
    
    // Returns list where start and end are given
    public ArrayList<Show> getSlist(String start, String end) {
        
               
        //If neither start or end is given, return all
        if("".equals(start) && "".equals(end)){
            return slist;
        }
        //If start is given, but end isn't, return all after start time
        else if(!"".equals(start) && "".equals(end)){
            int hUser = Integer.parseInt(start.substring(0, 2));
            int mUser = Integer.parseInt(start.substring(3, 5));
            

            for(int i = slist.size()-1; i > -1; i--){
                int hShow = Integer.parseInt(slist.get(i).getStart().substring(11,13));
                int mShow = Integer.parseInt(slist.get(i).getStart().substring(14,16));
                        
                if(hUser > hShow){
                    slist.remove(i);
                }
                else if(hUser == hShow && mUser > mShow){
                    slist.remove(i);
                }
                else{
                    //Do nothing
                }
            }
            return slist;
        }
        //If end is given, but start isn't, return all before end time
        else if("".equals(start) && !"".equals(end)){
            int hUser = Integer.parseInt(end.substring(0, 2));
            int mUser = Integer.parseInt(end.substring(3, 5));
            
            for(int i = slist.size()-1; i > -1; i--){
                int hShow = Integer.parseInt(slist.get(i).getEnd().substring(11,13));
                int mShow = Integer.parseInt(slist.get(i).getEnd().substring(14,16));
                        
                if(hUser < hShow){
                    slist.remove(i);
                }
                else if(hUser == hShow && mUser < mShow){
                    slist.remove(i);
                }
                else{
                    //Do nothing
                }
            }
            return slist;
        }
        //If both start and end are given, return all after start and before end
        else{
            int hUserStart = Integer.parseInt(start.substring(0, 2));
            int mUserStart = Integer.parseInt(start.substring(3, 5));
            
            int hUserEnd = Integer.parseInt(end.substring(0, 2));
            int mUserEnd = Integer.parseInt(end.substring(3, 5));
            
            for(int i = slist.size()-1; i > -1; i--){
                int hShowStart = Integer.parseInt(slist.get(i).getStart().substring(11,13));
                int mShowStart = Integer.parseInt(slist.get(i).getStart().substring(14,16));
                
                int hShowEnd = Integer.parseInt(slist.get(i).getEnd().substring(11,13));
                int mShowEnd = Integer.parseInt(slist.get(i).getEnd().substring(14,16));
                
                if(hUserStart > hShowStart){
                    slist.remove(i);
                }
                else if(hUserEnd < hShowEnd){
                    slist.remove(i);
                }
                else if(hUserStart == hShowStart && mUserStart > mShowStart){
                    slist.remove(i);
                }
                else if(hUserEnd == hShowEnd && mUserEnd < mShowEnd){
                    slist.remove(i);
                }
                else{
                    //Do nothing
                }
            }
            return slist;
        }
    }
    
    // Returns list where start, end and title are given
    public ArrayList<Show> getSlist(String start, String end, String title){
        
        slist = getSlist(start,end);
        
        for(int i = slist.size()-1; i > -1; i--){
            if(slist.get(i).getTitle() == null ? title != null : !slist.get(i).getTitle().equals(title)){
                slist.remove(i);
            }
        }
        
        return slist;
    }
    
    
    //Edits Show info to printable form if name is given
    public ArrayList<Show> editName(ArrayList<Show> list){
        
        for(Show x : list){
           x.setTitle(null);
           x.setStart("Alkaa: " + x.getStart());
           x.setEnd("\nLoppuu: " + x.getEnd());
        }
        
        return list;
    }
    
    //Edits Show info to printable form if name is not given
    public ArrayList<Show> editNoName(ArrayList<Show> list){
        for(Show x : list){
           x.setStart("\nAlkaa: " + x.getStart());
           x.setEnd("\nLoppuu: " + x.getEnd());
        }
            
        return list;
    }
}


class Show{
    private String title;
    private String start;
    private String end;
    private String theater;

    
    public Show(String t, String s, String e, String th){
        title = t;
        start = s;
        end = e;
        theater = th;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }
    
    @Override
    public String toString(){
        if(this.title == null){
            return theater + start + end;
        }
        else if(this.start == null){
            return title;
        }
        else{
            return theater + title + start + end;
        }
    }

    public String getTitle() {
        return title;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getTheater() {
        return theater;
    }
    
    public void setTheater(String theater) {
        this.theater = theater;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
}