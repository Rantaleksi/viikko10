/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko10;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Aleksi
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private ComboBox<Theater> theaterCombo;
    @FXML
    private TextField startField;
    @FXML
    private TextField endField;
    @FXML
    private TextField dateField;
    @FXML
    private Button listButton;
    @FXML
    private TextField nameField;
    @FXML
    private Button nameButton;
    @FXML
    private ListView<Show> movieList;
    @FXML
    private TextArea console;
    
    //Theaters and shows created
    Theaters theaters = new Theaters();
    Shows shows = new Shows();
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // Adds all theaters to the combobox
        theaterCombo.getItems().addAll(theaters.getAlist());
        
    }    

    //When query button is pressed
    @FXML
    private void getMovies(ActionEvent event) throws IOException {
        
        // If theater is not selected load all shows according to other parameters
        if(theaterCombo.getValue() == null){
            shows.load("", dateField.getText(), theaters.getAlist());
            movieList.getItems().clear();
            movieList.getItems().addAll(shows.editNoName(shows.getSlist(startField.getText(),endField.getText())));
        }
        // Else add theater to parameters
        else{
            shows.load(theaterCombo.getValue().getId(), dateField.getText(), theaters.getAlist());
            movieList.getItems().clear();
            movieList.getItems().addAll(shows.editNoName(shows.getSlist(startField.getText(),endField.getText())));
        }

    }

    // When query button with movie name is pressed
    @FXML
    private void nameFilter(ActionEvent event) throws IOException {
        
        //Same rules as before
        if(theaterCombo.getValue() == null){
            shows.load("", dateField.getText(), theaters.getAlist());
        }
        else{
            shows.load(theaterCombo.getValue().getId(), dateField.getText(), theaters.getAlist());
        }
        
        //Movie list is cleared, and then filled with new content 
        movieList.getItems().clear();
        movieList.getItems().add(new Show(nameField.getText(),"","",""));
        movieList.getItems().addAll(shows.editName(shows.getSlist(startField.getText(),endField.getText(),nameField.getText())));
    }
    
}